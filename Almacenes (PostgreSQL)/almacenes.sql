--
-- Creación de DB almacenes
--

CREATE DATABASE IF NOT EXISTS "almacenes";


--
-- Creación e inserción de la tabla Cajeros
--

CREATE TABLE "Cajeros" (
	"cajero" int NOT NULL,
	"nomApels" varchar NOT NULL,
	PRIMARY KEY ("cajero")
);

INSERT INTO "Cajeros" ("cajero", "nomApels")
		VALUES (1, 'Luis'), (2, 'Linda'), (3, 'Arleth'), (4, 'Renato'), (5, 'José Manuel');


--
-- Creación e inserción de la tabla Productos
--

CREATE TABLE "Productos" (
	"producto" int NOT NULL,
	"nombre" varchar NOT NULL,
	"precio" money NOT NULL,
	PRIMARY KEY ("producto")
);

INSERT INTO "Productos" ("producto", "nombre", "precio")
		VALUES (1, 'Gansito', 12.50), (2, 'Suavitel 1LT', 18.75), (3, 'Papel Higiénico', 20.90), (4, 'Desodorante', 34.00), (5, 'Refresco 2.5 LTS', 36.20), (6, 'Smart TV Samsung 55"', 8599.00);


--
-- Creación e inserción de la tabla Maquinas Registradoras
--

CREATE TABLE "Maquinas_Registradoras" (
	"maquina" int NOT NULL,
	"piso" int NOT NULL,
	PRIMARY KEY ("maquina")
);

INSERT INTO "Maquinas_Registradoras" ("maquina", "piso") 
		VALUES (1, 1), (2, 2), (3, 3), (4, 1), (5, 2), (6, 3);


--
-- Creación e inserción de la tabla Venta
--

CREATE TABLE "Venta" (
	"cajero" int NOT NULL,
	"maquina" int NOT NULL,
	"producto" int NOT Null
);

ALTER TABLE "Venta"
  ADD CONSTRAINT "fk_venta_cajero" FOREIGN KEY ("cajero") REFERENCES "Cajeros" ("cajero") ON UPDATE CASCADE,
  ADD CONSTRAINT "fk_venta_maquina" FOREIGN KEY ("maquina") REFERENCES "Maquinas_Registradoras" ("maquina") ON UPDATE CASCADE,
  ADD CONSTRAINT "fk_venta_producto" FOREIGN KEY ("producto") REFERENCES "Productos" ("producto") ON UPDATE CASCADE;

INSERT INTO "Venta" ("cajero", "maquina", "producto") 
		VALUES (1, 1, 3), (1, 2, 2), (1, 3, 1),
			   (2, 4, 5), (2, 5, 4), (2, 6, 6),
			   (3, 1, 4), (3, 2, 4), (3, 3, 5),
			   (4, 4, 4), (4, 5, 6), (4, 6, 5),
			   (5, 1, 4), (5, 2, 4), (5, 3, 6);

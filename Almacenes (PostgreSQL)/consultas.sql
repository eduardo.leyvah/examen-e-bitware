-- Mostrar el número de ventas de cada producto, ordenado de más a menos ventas. --

SELECT
	"Productos".producto AS ID,
	"Productos".nombre AS Nombre,
	COUNT("Venta".producto) AS Total_de_Ventas
FROM
	"Venta"
	JOIN "Productos" ON "Productos".producto = "Venta".producto
GROUP BY
	"Productos".producto
ORDER BY
	COUNT("Venta".producto)
	DESC;



-- Obtener un informe completo de ventas, indicando el nombre del cajero que realizo la venta, nombre y precios de los productos vendidos, y el piso en el que se encuentra la máquina registradora donde se realizó la venta. --

SELECT
	"Cajeros"."nomApels" AS Nombre_cajero,
	"Productos".nombre AS Producto,
	"Productos".precio AS Precio,
	"Maquinas_Registradoras".piso AS Piso
FROM
	"Venta"
	JOIN "Cajeros" ON "Cajeros".cajero = "Venta".cajero
	JOIN "Productos" ON "Productos".producto = "Venta".producto
	JOIN "Maquinas_Registradoras" ON "Maquinas_Registradoras".maquina = "Venta".maquina;



--  Obtener las ventas totales realizadas en cada piso. --
SELECT
	COUNT("Venta".maquina) AS Total_Ventas,
	"Maquinas_Registradoras".piso
FROM
	"Venta"
	JOIN "Maquinas_Registradoras" ON "Maquinas_Registradoras".maquina = "Venta".maquina
GROUP BY
	"Maquinas_Registradoras".piso
ORDER BY
	"Maquinas_Registradoras".piso ASC;



-- Obtener el código y nombre de cada cajero junto con el importe total de sus ventas. --
SELECT
	"Cajeros".cajero AS Codigo,
	"Cajeros"."nomApels" AS Nombre,
	COUNT("Venta".cajero) AS Total_Ventas
FROM
	"Venta"
	JOIN "Cajeros" ON "Cajeros".cajero = "Venta".cajero
GROUP BY
	"Cajeros".cajero
ORDER BY
	"Cajeros".cajero ASC;



-- Obtener el código y nombre de aquellos cajeros que hayan realizado ventas en pisos cuyas ventas totales sean inferiores a los 5000 pesos. --
SELECT
	"Cajeros".cajero AS Codigo,
	"Cajeros"."nomApels" AS Nombre,
	SUM("Productos".precio) AS Total_Vendido
FROM
	"Venta"
	JOIN "Cajeros" ON "Cajeros".cajero = "Venta".cajero
	JOIN "Productos" ON "Productos".producto = "Venta".producto
GROUP BY
	"Cajeros"."cajero"
HAVING
	SUM("Productos".precio) < '5000'
ORDER BY
	"Cajeros".cajero ASC;

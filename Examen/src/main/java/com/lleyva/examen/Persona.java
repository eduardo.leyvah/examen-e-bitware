package com.lleyva.examen;

/**
 * Clase persona
 * 
 * @author eduardo leyva herrera
 */

public class Persona {
    
    // CONSTANTES
    public static final String SEXO = "H";

    // Debajo de su peso ideal
    public static final int POCO_PESO = -1;
 
    // Peso ideal
    public static final int PESO_IDEAL = 0;
 
    // Sobrepeso
    public static final int SOBREPESO = 1;
    
    // ATRIBUTOS
    private String nombre = "";
    private int edad = 0;
    private String nss;
    private String sexo = SEXO;
    private double peso = 0;
    private double altura = 0;
    
    /**
     * @param nombre de la persona
     * @param edad de la persona
     * @param nss de la persona
     * @param sexo de la persona
     * @param peso de la persona
     * @param altura de la persona
     */
    
    public Persona(String nombre, int edad, String nss, String sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.nss = nss;
        this.sexo = comprobarSexo(sexo);
        this.peso = peso;
        this.altura = altura;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }
    
    public boolean esMayorDeEdad() {
        return (this.edad >= 18);
    }
    
    public String comprobarSexo(String sexo){
        if (!sexo.equals("H") && !sexo.equals("M")) {
            sexo = SEXO;
        }
        return sexo;
    }
    
    public int calcularIMC() {
        double peso = this.peso / (Math.pow(this.altura, 2));
        //Segun el peso, devuelve un codigo
        if (peso >= 20 && peso <= 25) {
            return PESO_IDEAL;
        } else if (peso < 20) {
            return POCO_PESO;
        } else {
            return SOBREPESO;
        }
    }
    
    @Override
    public String toString() {
        String sexo = this.sexo;
        if (sexo.equals("H")) {
            sexo = "Hombre";
        } else {
            sexo = "Mujer";
        }
        return "Nombre: " + nombre + "\n"
                + "Sexo: " + sexo + "\n"
                + "Edad: " + edad + " años\n"
                + "NSS: " + nss + "\n"
                + "Peso: " + peso + " kg\n"
                + "Altura: " + altura + " metros\n";
    }
}

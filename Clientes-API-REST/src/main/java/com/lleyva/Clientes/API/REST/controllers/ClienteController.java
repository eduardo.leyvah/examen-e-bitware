package com.lleyva.Clientes.API.REST.controllers;

import com.lleyva.Clientes.API.REST.models.ClienteModel;
import com.lleyva.Clientes.API.REST.repositories.IClientesDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author eduardolh
 */

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/")
public class ClienteController {
    
    @Autowired
    private IClientesDAO clienteRepository;
    
    @GetMapping("/clientes")
    public List getClientes(){
        HashMap<String, String> map = new HashMap<>();
        
        List clientes = clienteRepository.findAll();
        
        if (clientes.isEmpty()) {
            map.put("Cve_Error", "-1");
            map.put("Cve_Mensaje", "No hay clientes");
            clientes.add(map);
            return clientes;
        }
        map.put("Cve_Error", "0");
        map.put("Cve_Mensaje", "Listado de clientes");
        clientes.add(map);
        return clientes;
    }
    
    @GetMapping("/clientes/{clienteId}")
    public List getClienteId(@PathVariable("clienteId") Long id){
        HashMap<String, String> map = new HashMap<>();
        List clientes = new ArrayList();
        
        Optional<ClienteModel> cliente = clienteRepository.findById(id);
        
        if (cliente.isPresent()) {
            map.put("Cve_Error", "0");
            map.put("Cve_Mensaje", "Cliente encontrado");
            clientes.add(map);
            clientes.add(cliente);
            return clientes;
        }else {
            map.put("Cve_Error", "-1");
            map.put("Cve_Mensaje", "No existe el cliente");
            clientes.add(map);
            return clientes;
        }
        
    }
    
    
    @PostMapping("/crear")
    public List crearCliente(@Validated @RequestBody ClienteModel cliente){
        HashMap<String, String> map = new HashMap<>();
        List clientes = new ArrayList();
        
        try {
            if( validaDuplicados(cliente) ){
               map.put("Cve_Error", "-1");
               map.put("Cve_Mensaje", "El nombre de usuario o email ya se encuentran registrados");
               clientes.add(map);
               return clientes;
            }else{
                clienteRepository.insert(cliente);
                map.put("Cve_Error", "0");
                map.put("Cve_Mensaje", "Cliente creado");
                clientes.add(map);
                clientes.add(cliente);
                return clientes;
            }
        } catch (Exception e) {
            clientes.add(error(e.getMessage().split(":")[0]));
            return clientes;
        }
    }
    
    @PutMapping("cliente/update/{clienteId}")
    public List actualizarCliente(@PathVariable("clienteId") Long id, @Validated @RequestBody ClienteModel cliente){
        HashMap<String, String> map = new HashMap<>();
        List clientes = new ArrayList();
               
        try {
            
          if( validaDuplicados(cliente)){
               map.put("Cve_Error", "-1");
               map.put("Cve_Mensaje", "El nombre de usuario o email ya se encuentran registrados");
               clientes.add(map);
               return clientes;
          }else{
                clienteRepository.save(cliente);
                map.put("Cve_Error", "0");
                map.put("Cve_Mensaje", "Cliente actualizado correctamente");
                clientes.add(map);
                clientes.add(cliente);
                return clientes;
          }
          
        } catch (Exception e) {
          clientes.add(error(e.getMessage().split(":")[0]));
          return clientes;
        }
       
    }
    
    private boolean validaDuplicados(ClienteModel cliente){
        
        Optional<ClienteModel> clienteEmail = clienteRepository.findByEmail(cliente.getEmail());
        Optional<ClienteModel> clienteNombreUsuario = clienteRepository.findByNombreUsuario(cliente.getNombreUsuario());
        
        return ( clienteNombreUsuario.isPresent() || clienteEmail.isPresent() );
        
    }
    

    private Map<String, String> error(String error) {
        HashMap<String, String> map = new HashMap<>();
        if (error.contains("E11000 duplicate key error collection")) {
            map.put("Cve_Error", "-1");
            map.put("Cve_Mensaje", "El ID se encuentra duplicado");
            return  map;
        }
        map.put("Cve_Error", "-2");
        map.put("Cve_Mensaje", "La información no ha sido guardada.");
        return map;
    }
    
}

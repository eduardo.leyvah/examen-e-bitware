
package com.lleyva.Clientes.API.REST.repositories;

import com.lleyva.Clientes.API.REST.models.ClienteModel;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author eduardolh
 */

@Repository
public interface IClientesDAO extends MongoRepository<ClienteModel, Long>{
    
    Optional<ClienteModel> findByNombreUsuario(String nombreUsuario);
    Optional<ClienteModel> findByEmail(String email);
    
}

package com.lleyva.Clientes.API.REST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication

public class ClientesApiRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientesApiRestApplication.class, args);
	}

}
